export const apiDataFetch = url => fetch(url)
        .then(res => res.json())
        .then(data => data);
