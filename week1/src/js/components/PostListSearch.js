import React from '../dom/dom.js';

import PostListItem from './PostListItem.js';

const PostList = (posts, query) => {
    const children = posts.
    filter(item => item.title.includes(query) || item.body.includes(query))
    .slice(0, 10)
    .map(({ id, title, body } = post) => 
        PostListItem({ id, title, body })
    );

    const element = React.createElement('ul', {
        attributes: {
            class: 'PostList',
        },
        children: children,
    });

    return element;
};

export default PostList;
