import React from '../dom/dom.js';

const PostListSearch = () => {

    const $searchInput = React.createElement('input', {
        attributes: {
            type: 'text',
            placeholder: 'Search...',
            class: 'PostListSearch__input',
            value: '',
        },
        children: []
    });

    const $searchButton = React.createElement('button', {
        attributes: {
            type: 'submit',
            class: 'PostListSearch__button',
        },
        children: ['Search']
    });

    const $search = React.createElement('div', {
        attributes: {
            class: 'PostListSearch'
        },
        children: [
            $searchInput,
            $searchButton,
        ]
    });

   return $search;

};

export default PostListSearch;