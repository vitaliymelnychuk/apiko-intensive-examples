export const API_URL = 'https://jsonplaceholder.typicode.com/posts';
export const POST_LIMIT = 10;
export const INITIAL_PAGE = 1;