export const render = (target, element) => target.appendChild(element);
export const mount = (target, newElement, oldElement) => target.replaceChild(newElement, oldElement);
  

  