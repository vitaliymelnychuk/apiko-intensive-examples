import { createElement } from './createElement.js';
import { render, mount } from './renderElement.js';

const React = {
    createElement,
    render,
    mount,
};

export default React;