import React from './js/dom/dom.js';

import './styles/main.scss';
import PostList from './js/components/PostList.js';
//import PostListItem from './js/components/PostListItem.js';
import MoreButton from './js/components/MoreButton.js';
import PostListSearch from './js/components/PostListSearch.js';
import Search from './js/components/Search.js';

import { apiDataFetch } from './js/api.js';
import {API_URL, POST_LIMIT } from './js/consts.js';

const $root = document.getElementById('root');
const $moreButton = MoreButton();
const $search = Search();

let limit = POST_LIMIT;

const incrementLimit = () => limit+= POST_LIMIT;

const getContent = async limit => await apiDataFetch(`${API_URL}?_limit=${limit}`);

const submitQuery = async () => {

    limit = POST_LIMIT;

    const $input = document.querySelector('.PostListSearch__input');

    const query = $input.value;

    const searchPosts = await getContent(100);
    const oldPostList = document.querySelector('.PostList');
    const newPostList = PostListSearch(searchPosts, query); 
    React.mount($root, newPostList, oldPostList);
};


const init = async() => {

    React.render($root, $search);
    const $searchBtn = document.querySelector('.PostListSearch__button');
    $searchBtn.addEventListener('click', submitQuery, true);

    const currentPosts = await getContent(limit);
    const postList = PostList(currentPosts);
    React.render($root, postList);

    React.render($root, $moreButton);

    const $DOMbutton = document.querySelector('.MoreButton');
    $DOMbutton.addEventListener('click', showMore, true);
}

init();

const showMore = async () => {
    if ( limit >= 100){
        $moreButton.style.display = 'none';
    } else {
        incrementLimit();
        const posts = await getContent(limit);

        const oldPostList = document.querySelector('.PostList');
        const newPostList = PostList(posts);

        React.mount($root, newPostList, oldPostList);

    }
 };

