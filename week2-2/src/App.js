import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { AuthContext } from './contex/AuthContext'
import { useAuth } from './modules/auth/useAuth'
import { useAppInit } from './modules/app/useAppInit'
import { AppRouter } from './routes/AppRouter'

function App() {
  const auth = useAuth()
  const { isLoad } = useAppInit(auth)

  if (isLoad) return <div>Loading...</div>

  return (
    <BrowserRouter>
      <AuthContext.Provider value={auth}>
        <AppRouter isAuth={auth.state.isAuth} />
      </AuthContext.Provider>
    </BrowserRouter>
  )
}

export default App
