import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Home } from './../scenes/Home/Home'
import { Auth } from './../scenes/auth/Auth'
import { routes } from './routes'
import { isAuthRedirect } from './../utils/auth/isAuthRedirect'

export const AppRouter = ({ isAuth }) => {
  return (
    <Switch>
      <Route
        path={routes.auth.path}
        render={() => isAuthRedirect(Auth, isAuth)}
      />
      <Route exact path={routes.home.path} component={Home} />
    </Switch>
  )
}
