export const routes = {
  
  home: {
    path:'/',
    name: 'Home'
  },

  auth: {
    path: '/auth',
  },
  register: {
    path: '/auth/register',
    name: 'Register',
  },
  login: {
    path: '/auth/login',
    name: 'Login',
  },
}
