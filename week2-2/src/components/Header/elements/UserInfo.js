import React, { useState } from 'react'
import s from './userInfo.module.css'

export const UserInfo = ({ user, logout }) => {
  const [name, surname] = user.fullName.split(' ')
  const [isDetail, setDetail] = useState(false)
  return (
    <>
      <div className={s.circle} onClick={() => setDetail(!isDetail)}>
        {name[0] + surname[0]}
      </div>
      {isDetail && (
        <div className={s.detailInfo}>
          <div>
            <div className={s.blockInfo}>
              <div className={s.circle + ' ' + s.infoCircle}>
                {name[0] + surname[0]}
              </div>
              <div className={s.userInfo}>
                <p className={s.fullName}>{user.fullName}</p>
                <p className={s.email}>{user.email}</p>
                <p className={s.profile}>Profile</p>
              </div>
            </div>
            <div className={s.link}>Edit profile</div>

            <button className={s.link} onClick={logout}>
              Logout
            </button>
          </div>
        </div>
      )}
    </>
  )
}

UserInfo.defaultProps = {
  user: {
    email: '',
    fullName: '',
  },
}
