import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { Icon } from './../Icons/Icon'
import s from './header.module.css'
import { routes } from './../../routes/routes'
import { UserInfo } from './elements/UserInfo'
import { AuthContext } from './../../contex/AuthContext'

export const SearchHeader = () => {
  const auth = useContext(AuthContext)
  return (
    <header className={s.searchHeaderContainer}>
      <div className={s.header}>
        <div className={s.logo}>
          <Icon name="logo" fill="white" />
        </div>
        <div>
          <button className={s.headerBtn}>sell</button>
        </div>
        <div>
          {auth.state.isAuth ? (
            <UserInfo user={auth.state.user} logout={auth.logout} />
          ) : (
            <Link
              to={routes.login.path}
              className={`${s.link} ${s.searchLink}`}>
              {routes.login.name}
            </Link>
          )}
        </div>
        <div>
          <Icon name="heart" fill="white" />
        </div>
      </div>
    </header>
  )
}
