import React from "react";
import {Link} from 'react-router-dom'
import { Icon } from "./../Icons/Icon";
import s from "./header.module.css";
import {routes} from './../../routes/routes'

export const Header = () => {
  return (
    <header className={s.header}>
      <div className={s.logo}>
        <Icon name="logo" fill="#262525" />
      </div>
      <div>
        <button className={s.headerBtn}>sell</button>
      </div>
      <div>
        <Link to={routes.login.path} className={s.link}>
          {routes.login.name}
        </Link>
      </div>
      <div>
        <Icon name="heart" fill="#2E2E2E"/>
      </div>
    </header>
  );
};
