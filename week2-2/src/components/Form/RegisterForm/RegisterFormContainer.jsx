import React, { useContext } from 'react'
import { RegisterFormComponent } from './RegisterFormComponent'
import { AuthContext } from './../../../contex/AuthContext'

export const RegisterForm = () => {
  const { state, userRegister } = useContext(AuthContext)
  const handleSubmit = ({ email, password, fullName }) => {
    userRegister({ email, password, fullName })
  }

  return <RegisterFormComponent handleSubmit={handleSubmit} errorApi={state.error} />
}
