import React, {useContext} from 'react'
import { LoginFormComponent } from './LoginFormComponent'
import {AuthContext} from './../../../contex/AuthContext'

export const LoginForm = () => {
  const {state, userLogin } = useContext(AuthContext)
  const handleSubmit = ({ email, password }) => {
    userLogin({ email, password })
  }

  return <LoginFormComponent handleSubmit={handleSubmit} errorApi={state.error} />
}
