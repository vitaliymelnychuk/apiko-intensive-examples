import React from 'react'
import { Redirect } from 'react-router-dom'
import { routes } from '../../routes/routes'

export const isAuthRedirect = (Component, isAuth) => {
  return !isAuth ? <Component /> : <Redirect to={routes.home.path} />
}
