import axios from 'axios'
import { localStorage } from './../utils/localStorage/localStorage'

export const Api = {
  _token: null,

  init() {
    try {
      const token = localStorage.token.get()
      this.setToken(token)
    } catch (e) {
      console.log(e)
    }
  },

  _axiosSetToken(token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  },

  setToken(token) {
    this._token = token
    localStorage.token.set(token)
    this._axiosSetToken(token)
  },

  removeToken() {
    this._token = null
    this._axiosSetToken(null)
    localStorage.token.remove()
  },

  auth: {
    register: ({ email, password, fullName }) =>
      axios.post('/api/auth/register', { email, password, fullName }),

    login: ({ email, password }) =>
      axios.post('/api/auth/login', { email, password }),

    getAccountUser: () => axios.get('/api/account/user'),
  },
}
