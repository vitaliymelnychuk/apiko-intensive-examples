import React from 'react'
import { SearchHeader } from './../../components/Header/SearchHeader'
import { Footer } from './../../components/Footer/Footer'
import s from './home.module.css'

export const Home = () => {
    return (
        <div className={s.container}>
          <SearchHeader/>
          <main className={s.main}>
           
          </main>
          <Footer />
        </div>
      )
}
