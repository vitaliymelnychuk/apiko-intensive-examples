import { useEffect } from 'react'
import {
  appReducer,
  successActionCreator,
  errorActionCreator,
} from './appReducer'
import { useReducer } from 'react'
import { Api } from '../../api/Api'

export const useAppInit = (auth) => {
  const [state, dispatch] = useReducer(appReducer, {
    isLoad: true,
    isError: false,
    error: null,
  })
  
  useEffect(() => {
    async function init() {
      try {
        Api.init()
        await auth.isTokenValid()
        await dispatch(successActionCreator())
      } catch (e) {
        dispatch(errorActionCreator(e))
      }
    }
    init()
  }, [])

  return {
    ...state,
  }
}
