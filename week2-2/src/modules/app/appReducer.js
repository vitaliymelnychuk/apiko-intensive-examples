const APP_ACTIONS = {
  SUCCESS: 'success',
  ERROR: 'error',
}

export const successActionCreator = () => {
  return {
    type: APP_ACTIONS.SUCCESS,
  }
}

export const errorActionCreator = (error) => {
  return {
    type: APP_ACTIONS.SUCCESS,
    payload: error,
  }
}

export const appReducer = (state, action) => {
  const { type, payload } = { ...action }

  switch (type) {
    case APP_ACTIONS.SUCCESS:
      return {
        ...state,
        isLoad: false,
        isError: false,
        error: null,
      }
    case APP_ACTIONS.ERROR:
      return {
        ...state,
        isLoad: false,
        isError: true,
        error: payload,
      }
    default:
      throw new Error('Auth action is not defined')
  }
}
