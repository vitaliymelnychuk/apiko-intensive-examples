const AUTH_ACTIONS = {
  START: "start",
  LOGIN: 'add_user',
  LOGOUT: 'remove_user',
  ERROR: 'error',
}

export const loginActionCreator = (user) => {
  return {
    type: AUTH_ACTIONS.LOGIN,
    payload: user,
  }
}

export const logoutActionCreator = () => {
  return {
    type: AUTH_ACTIONS.LOGOUT,
  }
}

export const errorActionCreator = (error) => {
  return {
    type: AUTH_ACTIONS.ERROR,
    payload: error,
  }
}

export const startActionCreator = () =>{
  return {
    type: AUTH_ACTIONS.START,
  }
}

export const authReducer = (state, action) => {
  const { type, payload } = { ...action }

  switch (type) {
    case AUTH_ACTIONS.START:
      return {
        ...state,
        isLoad: true,
        isAuth: false,
        isError: false,
        error: null
      }
    case AUTH_ACTIONS.LOGIN:
      return {
        ...state,
        isLoad: false,
        user: payload,
        isAuth: true,
        isError: false,
        error: null
      }
    case AUTH_ACTIONS.LOGOUT:
      return {
        ...state,
        user: null,
        isAuth: false,
        isError: false,
        error: null
      }
      case AUTH_ACTIONS.ERROR:
      return {
        ...state,
        isLoad: false,
        isError: true,
        error: payload
      }
    default:
      throw new Error('Auth action is not defined')
  }
}
