import {
  authReducer,
  loginActionCreator,
  startActionCreator,
  errorActionCreator,
  logoutActionCreator,
} from './authReducer'
import { useReducer } from 'react'
import { Api } from './../../api/Api'

export const useAuth = () => {
  const [state, dispatch] = useReducer(authReducer, {
    isLoad: false,
    user: null,
    isAuth: false,
    isError: false,
    error: null,
  })

  const userLogin = async ({ email, password }) => {
    try {
      dispatch(startActionCreator())
      const response = await Api.auth.login({ email, password })
      dispatch(loginActionCreator(response.data.user))
      Api.setToken(response.data.token)
    } catch (e) {
      dispatch(errorActionCreator(e.response.data.error))
    }
  }

  const userRegister = async ({ email, fullName, password }) => {
    try {
      dispatch(startActionCreator())
      const response = await Api.auth.register({ email, fullName, password })
      dispatch(loginActionCreator(response.data.user))
      Api.setToken(response.data.token)
    } catch (e) {
      dispatch(errorActionCreator(e.response.data.error))
    }
  }
  const isTokenValid = async () => {
    try {
      dispatch(startActionCreator())
      const response = await Api.auth.getAccountUser()
      dispatch(loginActionCreator(response.data))
    } catch (e) {
      logout()
    }
  }

  const logout = () => {
    Api.removeToken()
    dispatch(logoutActionCreator())
  }

  return {
    state,
    userLogin,
    userRegister,
    isTokenValid,
    logout,
  }
}
