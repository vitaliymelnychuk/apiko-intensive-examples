# 8th-react-intensive-best-examples

## Week 1 by Savkiv Olena

Gitlab profile: https://gitlab.com/el.dombrovan
Code folder `./week1/`

## Week 2 by Dolinovskyi Roman

Gitlab profile: https://gitlab.com/RomanDolinovskyi
Code folder `./week2-1/`

## Week 2 by Hatsyur Andriy

Gitlab profile: https://gitlab.com/andriy.hatsyur
Code folder `./week2-2/`
