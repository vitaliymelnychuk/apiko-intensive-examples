import React from "react";
import { v4 as uuid } from "uuid";

import { useFormik } from "formik";

import * as Api from "./../../../Api/Api";

import LoginForm from "./LoginForm/LoginForm";

import s from "./../WrapperAuth.module.scss";
import Schema from "./LoginValidationSchema";
import { Link, useHistory } from "react-router-dom";
import routes from "../../../router/router";

const Login = ({ setUser }) => {
  const _id = uuid();
  const history = useHistory();
  const formik = useFormik({
    validationSchema: Schema,
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit({ email, password }, actions) {
      Api.Authentificaton.login(email, password)
        .then((user) => setUser(user))
        .then(() => history.push(routes.HOME))
        .catch((err) =>
          actions.setFieldError("password", "Incorrect password or email")
        );
    },
  });

  return (
    <>
      <LoginForm {...{ _id, formik }} />
      <div className={s.helpLink}>
        I have no account,{" "}
        <Link to={routes.REGISTER}>
          <span>register now</span>
        </Link>
      </div>
    </>
  );
};

export default Login;
