import React from "react";

import Email from "../../components/emai.jsx";
import Password from "../../components/password.jsx";

const LoginForm = ({ _id, formik }) => {
  return (
    <form onSubmit={formik.handleSubmit}>
      <legend>Login</legend>

      <Email {...{ formik }} />
      <Password
        {...{ _id, formik }}
        type="password"
        title="Password"
        forgetPass="true"
      />

      <input type="submit" value="Continue" />
    </form>
  );
};

export default LoginForm;
