import React from "react";
import { v4 as uuid } from "uuid";
import { useFormik } from "formik";

import RegisterForm from "./RegisterForm/RegisterForm";

import * as Api from "./../../../Api/Api";
import Schema from "./RegisterValidationSchema";

import s from "./../WrapperAuth.module.scss";
import { Link, useHistory } from "react-router-dom";
import routes from "../../../router/router";

const Register = ({ setUser }) => {
  const _id1 = uuid();
  const _id2 = uuid();
  const history = useHistory();
  const formik = useFormik({
    validationSchema: Schema,
    initialValues: {
      email: "",
      fullName: "",
      password: "",
      repeatedPassword: "",
    },
    onSubmit({ email, fullName, password }) {
      Api.Authentificaton.register(email, password, fullName)
        .then((user) => setUser(user))
        .then(() => history.push(routes.HOME));
    },
  });
  return (
    <>
      <RegisterForm {...{ _id1, _id2, formik }} />
      <div className={s.helpLink}>
        I already have an account,{" "}
        <Link to={routes.LOGIN}>
          <span>log in</span>
        </Link>
      </div>
    </>
  );
};

export default Register;
