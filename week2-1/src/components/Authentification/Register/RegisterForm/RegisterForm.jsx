import React from "react";

import Email from "../../components/emai.jsx";
import FullName from "../../components/fullName.jsx";
import Password from "../../components/password.jsx";

const RegisterForm = ({ _id1, _id2, formik }) => {
  return (
    <form onSubmit={formik.handleSubmit}>
      <legend>Register</legend>

      <Email {...{ formik }} />
      <FullName {...{ formik }} />
      <Password {...{ _id: _id1, formik }} type="password" title="Password" />
      <Password
        {...{ _id: _id2, formik }}
        type="repeatedPassword"
        title="Password Again"
      />
      <input type="submit" value="Continue" />
    </form>
  );
};

export default RegisterForm;
