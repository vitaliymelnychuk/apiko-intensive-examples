import React from "react";

import s from "./Header.module.scss";
import { Icons, icon } from "../../utils/icons";
import { Link } from "react-router-dom";
import routes from "../../router/router";
import { UserContext } from "./../../App";

const Header = ({ logOut }) => {
  function onClick() {
    logOut();
  }

  return (
    <UserContext.Consumer>
      {(user) => (
        <div id={s.header}>
          <Link to={routes.HOME}>{Icons(icon._ApicoLogo, 102)}</Link>
          {user && <span>{user.fullName}</span>}
          {user ? (
            <button {...{ onClick }}>Logout</button>
          ) : (
            <Link to={routes.LOGIN}>
              <button>Login</button>
            </Link>
          )}
        </div>
      )}
    </UserContext.Consumer>
  );
};

export default Header;
