import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import WrapperAurh from "../../components/Authentification/WrapperAuth.jsx";
import Login from "../../components/Authentification/Login/Login.jsx";
import Register from "../../components/Authentification/Register/Register.jsx";
import routes from "../../router/router.js";

const Authentification = ({ user, setUser }) => {
  return (
    <div id="auth-page">
      {user && <Redirect to={routes.HOME} />}
      <WrapperAurh>
        <Switch>
          <Route
            path={routes.LOGIN}
            component={() => <Login {...{ setUser }} />}
          />
          <Route
            path={routes.REGISTER}
            component={() => <Register {...{ setUser }} />}
          />
        </Switch>
      </WrapperAurh>
    </div>
  );
};

export default Authentification;
