import React, { useEffect, createContext, useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import routes from "./router/router";

import Authentification from "./scenes/Authentification/Authentification.jsx";
import Home from "./scenes/Home/Home";
import * as Api from "./Api/Api";

import "./App.module.scss";
import Header from "./components/Header/Header";

export const UserContext = createContext();

function App() {
  const [user, setUser] = useState(null);

  function logOut() {
    Api.Authentificaton.logout();
    setUser(null);
  }

  useEffect(() => {
    Api.Authentificaton.initialization().then((res) => setUser(res));
  }, []);

  return (
    <UserContext.Provider value={user}>
      <BrowserRouter>
        <Header {...{ logOut }} />
        <Switch>
          <Route exact path={routes.HOME} component={Home} />
          <Route
            path={routes.AUTHENTIFICATION}
            render={() => <Authentification {...{ user, setUser }} />}
          />
        </Switch>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
