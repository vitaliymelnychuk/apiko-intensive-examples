const routes = {
  HOME: "/",
  AUTHENTIFICATION: "/auth",
  LOGIN: "/auth/login",
  REGISTER: "/auth/register",
};

export default routes;
