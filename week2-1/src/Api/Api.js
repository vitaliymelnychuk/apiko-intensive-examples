import axios from "axios";

import Storage from './storage'

export let _token = undefined;
export let user = null;

export const Authentificaton = {
  async login(email, password) {
    let { data } = await axios.post("/api/auth/login", {
      email,
      password,
    });
    this.initializeToken(data.token);
    user = data.user;
    return user;
  },

  async register(email, password, fullName) {
    let { data } = await axios.post("/api/auth/register", {
      fullName,
      email,
      password,
    });
    this.initializeToken(data.token);
    user = data.user;
    return user;
  },

  logout() {
    _token = null;
    user = null;
    Storage.removeToken();
    axios.defaults.headers.common["Authorization"] = undefined;
  },

  async initialization() {
    try {
      let storeToken = Storage.getToken();
      if(storeToken){
        this.initializeToken(storeToken);
        if (!!_token) {
          let { data } = await axios.get("/api/account");
          user = data;
          return user;
        }
      }
    } catch (err) {
      console.log(err);
    }
  },
  initializeToken(token) {
    _token = token;
    Storage.setToken(token);
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  },
};
