const Storage = {
  getToken() {
    return window.localStorage.getItem("_token");
  },
  setToken(token) {
    window.localStorage.setItem("_token", token);
  },
  removeToken(){
    window.localStorage.removeItem("_token");
  }
};

export default Storage;